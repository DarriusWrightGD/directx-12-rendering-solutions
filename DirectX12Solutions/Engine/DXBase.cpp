#include "DXBase.h"
#include <shellapi.h>


DXBase::DXBase(int width, int height, std::wstring name) :
	width(width), height(height), useWarpDevice(false)
{
	ParseCommandLineArgs();
	title = name + (useWarpDevice ? L" (WARP)" : L"");
	WCHAR assetsPath[512];
	GetAssetsPath(assetsPath, _countof(assetsPath));
	this->assetsPath = assetsPath;
	this->aspectRatio = static_cast<float>(width) / static_cast<float>(height);

}

DXBase::~DXBase()
{
}

int DXBase::Run(HINSTANCE hInstance, int nCmdShow)
{
	//Create the window class
	WNDCLASSEX windowClass = {0};
	windowClass.cbSize = sizeof(WNDCLASSEX);
	windowClass.style = CS_HREDRAW | CS_VREDRAW;
	windowClass.lpfnWndProc = MessageHandler;
	windowClass.hInstance = hInstance;
	windowClass.lpszClassName = L"WindowClass1";
	RegisterClassEx(&windowClass);
	RECT windowRect = { 0,0,static_cast<long>(width), static_cast<long>(height) };
	
	//create the window and store the handle 
	hWnd = CreateWindowEx(NULL,
		L"WindowClass1",
		title.c_str(),
		WS_OVERLAPPEDWINDOW,
		300,
		300,
		windowRect.right - windowRect.left,
		windowRect.bottom - windowRect.top,
		NULL,
		NULL,
		hInstance,
		NULL
		);

	ShowWindow(hWnd, nCmdShow);

	OnInit();

	MSG msg = { 0 };

	while (true)
	{
		//If there is a message remove it to be processed
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);

			if (msg.message == WM_QUIT)
				break;

			OnEvent(msg);
		}

		//once the message has been processed update and render the window
		OnUpdate();
		OnRender();
	}

	//clean up
	OnDestory();

	return static_cast<char>(msg.wParam);
}

void DXBase::SetTitle(LPCWSTR text)
{
	std::wstring windowText = text;
	SetWindowText(hWnd, windowText.c_str());
}

std::wstring DXBase::GetAssestFullPath(LPCWSTR assetName)
{
	return assetsPath + assetName;
}

LRESULT DXBase::MessageHandler(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	//handle destory/shutdown messages
	switch (message)
	{
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
		break;
	}

	// handle all messages that the switch statement missed
	return DefWindowProc(hWnd,message,wParam,lParam);
}

void DXBase::ParseCommandLineArgs()
{
	int argc;
	LPWSTR *argv = CommandLineToArgvW(GetCommandLineW(), &argc);
	for (int i = 1; i < argc; ++i)
	{
		if (_wcsnicmp(argv[i], L"-warp", wcslen(argv[i])) == 0 ||
			_wcsnicmp(argv[i], L"/warp", wcslen(argv[i])) == 0)
		{
			useWarpDevice = true;
		}
	}
	LocalFree(argv);
}
