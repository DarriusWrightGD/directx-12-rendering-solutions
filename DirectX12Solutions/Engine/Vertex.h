#pragma once
#include "DXHelper.h"
#include <DirectXMath.h>

struct ColorVertex
{
	DirectX::XMFLOAT3 position;
	DirectX::XMFLOAT4 color;
};

struct Vertex
{
	DirectX::XMFLOAT3 position;
};