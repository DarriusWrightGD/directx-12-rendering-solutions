#pragma once
#include "DXHelper.h"
#include "Export.h"

using namespace DirectX;

class DXBase
{
public:
	ENGINE_SHARED DXBase(int width, int height, std::wstring name);
	ENGINE_SHARED virtual ~DXBase();

	ENGINE_SHARED int Run(HINSTANCE hInstance, int nCmdShow);
	ENGINE_SHARED void SetTitle(LPCWSTR text);

protected:
	// should be called before processing messages, but after the window is shown
	virtual void OnInit() = 0; 
	//Each frame once the message has been processed the update function
	//should be called followed by the render function
	virtual void OnUpdate() = 0;
	virtual void OnRender() = 0;
	//Clean up
	virtual void OnDestory() = 0;
	//handle the message event 
	virtual bool OnEvent(MSG msg) = 0;

	ENGINE_SHARED std::wstring GetAssestFullPath(LPCWSTR assetName);
	ENGINE_SHARED static LRESULT CALLBACK MessageHandler(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

	UINT width;
	UINT height;
	float aspectRatio;
	HWND hWnd;
	bool useWarpDevice;

private:
	void ParseCommandLineArgs();
	std::wstring assetsPath;
	std::wstring title;

};

