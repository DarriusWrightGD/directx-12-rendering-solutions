#pragma once
#include "DXBase.h"

using namespace Microsoft::WRL;

class HelloTriangle : public DXBase
{
public:
	HelloTriangle(UINT width, UINT height, std::wstring name);

protected:
	// Inherited via DXBase
	virtual void OnInit() override;
	virtual void OnUpdate() override;
	virtual void OnRender() override;
	virtual void OnDestory() override;
	virtual bool OnEvent(MSG msg) override;

private:
	static const UINT FRAME_COUNT = 2; //maybe framebuffer count, or render targets 

	//Render Pipeline Assets
	D3D12_VIEWPORT viewport;
	D3D12_RECT scissorRect;

	//App resources
	ComPtr<ID3D12Resource> vertexBuffer;
	D3D12_VERTEX_BUFFER_VIEW vertexBufferView;


	//Pipeline objects
	ComPtr<IDXGISwapChain3> swapChain;
	ComPtr<ID3D12Device> device;
	ComPtr<ID3D12Resource> renderTargets[FRAME_COUNT];
	ComPtr<ID3D12CommandAllocator> commandAllocator;
	ComPtr<ID3D12CommandQueue> commandQueue;
	//A collection of contiguous allocations of descriptors
	ComPtr<ID3D12DescriptorHeap> rtvHeap; //what is this?
	ComPtr<ID3D12PipelineState> pipelineState;
	ComPtr<ID3D12GraphicsCommandList> commandList;
	ComPtr<IDXGIFactory4> factory;
	ComPtr<ID3D12RootSignature> rootSignature;

	UINT rtvDescriptorSize;

	//Synchronization objects
	UINT frameIndex;
	HANDLE fenceEvent;
	ComPtr<ID3D12Fence> fence;
	UINT64 fenceValue;

	//Initialization helper functions 
	void LoadPipeline();
	void LoadAssets();
	void PopulateCommnadList();
	void WaitForPreviousFrame();

	void InitDebugInterface();
	void InitDevice();
	void InitCommandQueue();
	void InitSwapChain();
	void InitDescriptorHeap();
	void InitCommandAllocator();
	void InitViewport();
	void InitRect();
	void InitRootSignature();
	void InitPipelineState();
	void SendVertexData();
	void InitFence();

};