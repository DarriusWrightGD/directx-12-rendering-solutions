// 02_HelloTriangle.cpp : Defines the entry point for the console application.
//


#include "stdafx.h"
#include "HelloTriangle.h"

_Use_decl_annotations_
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE, LPSTR, int nCmdShow)
{
	HelloTriangle sample(1080, 720, L"Hello Triangle");
	return sample.Run(hInstance, nCmdShow);
}
