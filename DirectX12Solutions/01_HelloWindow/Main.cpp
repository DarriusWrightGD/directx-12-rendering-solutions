// 01_HelloWindow.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "HelloWindow.h"

_Use_decl_annotations_
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE, LPSTR, int nCmdShow)
{
	HelloWindow sample(1080, 720, L"Hello Window");
	return sample.Run(hInstance,nCmdShow);
}
