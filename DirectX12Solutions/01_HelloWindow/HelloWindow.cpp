#include "stdafx.h"
#include "DXHelper.h"
#include "HelloWindow.h"
#include <Vertex.h>

HelloWindow::HelloWindow(UINT width, UINT height, std::wstring name) :
	DXBase(width, height, name)
{
}

void HelloWindow::OnInit()
{
	LoadPipeline();
	LoadAssets();
}

void HelloWindow::OnUpdate()
{
}

void HelloWindow::OnRender()
{
	//record all command that we need to renderer the scene into the command list
	PopulateCommnadList();

	ID3D12CommandList * commandLists[] = { commandList.Get() };
	commandQueue->ExecuteCommandLists(_countof(commandLists), commandLists);

	ThrowIfFailed(swapChain->Present(1, 0));
	WaitForPreviousFrame();
}

void HelloWindow::OnDestory()
{
	WaitForPreviousFrame();
	CloseHandle(fenceEvent);
}

bool HelloWindow::OnEvent(MSG msg)
{
	return false;
}

void HelloWindow::LoadPipeline()
{
	InitDebugInterface();
	InitDevice();
	InitCommandQueue();
	InitSwapChain();
	InitDescriptorHeap();
	InitCommandAllocator();
}

void HelloWindow::LoadAssets()
{
	//create the command list
	ThrowIfFailed(device->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, commandAllocator.Get(), nullptr, IID_PPV_ARGS(&commandList)));

	// Command lists are created in the recording state, but there is nothing
	// to record yet. The main loop expects it to be closed, so close it now.
	ThrowIfFailed(commandList->Close());

	// create synchronization objects 
	{
		ThrowIfFailed(device->CreateFence(0, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&fence)));
		fenceValue = 1;

		fenceEvent = CreateEventEx(nullptr, FALSE, FALSE, EVENT_ALL_ACCESS);
		if (fenceEvent == nullptr)
		{
			ThrowIfFailed(HRESULT_FROM_WIN32(GetLastError()));
		}
	}
}

void HelloWindow::PopulateCommnadList()
{
	// Command list allocators can only be reset when the associated 
	// command lists have finished execution on the GPU; apps should use 
	// fences to determine GPU execution progress.

	ThrowIfFailed(commandAllocator->Reset());
	ThrowIfFailed(commandList->Reset(commandAllocator.Get(), pipelineState.Get()));

	commandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(renderTargets[frameIndex].Get(), D3D12_RESOURCE_STATE_PRESENT, D3D12_RESOURCE_STATE_RENDER_TARGET));

	CD3DX12_CPU_DESCRIPTOR_HANDLE rtvHandle(rtvHeap->GetCPUDescriptorHandleForHeapStart(), frameIndex, rtvDescriptorSize);

	const float clearColor[] = { 0.3f, 0.2f, 0.4f, 1.0f };

	commandList->ClearRenderTargetView(rtvHandle, clearColor, 0, nullptr);
	commandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(renderTargets[frameIndex].Get(), D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PRESENT));

	ThrowIfFailed(commandList->Close());
}

void HelloWindow::WaitForPreviousFrame()
{
	// WAITING FOR THE FRAME TO COMPLETE BEFORE CONTINUING IS NOT BEST PRACTICE.
	// This is code implemented as such for simplicity. More advanced samples 
	// illustrate how to use fences for efficient resource usage.

	const UINT64 fence = this->fenceValue;
	ThrowIfFailed(commandQueue->Signal(this->fence.Get(), fence));

	this->fenceValue++;

	if (this->fence->GetCompletedValue() < fence)
	{
		ThrowIfFailed(this->fence->SetEventOnCompletion(fence, fenceEvent));
		WaitForSingleObject(fenceEvent, INFINITE);
	}
	frameIndex = swapChain->GetCurrentBackBufferIndex();
}

void HelloWindow::InitDebugInterface()
{
	#ifdef _DEBUG
	{

		//This used to validate pipeline state
		ComPtr<ID3D12Debug> debugController;
		//TODO find out how this works 
		//IID_PPV_ARGS - supplies two arguments to the funciton that are needed to create the 
		//debug interface it gets the id and the pointer to the object
		// IID - the id , PPV - the pointer , args - arugments 
		if (SUCCEEDED(D3D12GetDebugInterface(IID_PPV_ARGS(&debugController))))
		{
			debugController->EnableDebugLayer();
		}
	}
	#endif
}

void HelloWindow::InitDevice()
{
	//initialize the factory
	ThrowIfFailed(CreateDXGIFactory1(IID_PPV_ARGS(&factory)));
	//TODO find out what the different numbers mean on the classes
	if (useWarpDevice)
	{
		ComPtr<IDXGIAdapter> warpAdapter;
		ThrowIfFailed(factory->EnumWarpAdapter(IID_PPV_ARGS(&warpAdapter)));
		ThrowIfFailed(D3D12CreateDevice(warpAdapter.Get(), D3D_FEATURE_LEVEL_11_0, IID_PPV_ARGS(&device)));
	}
	else
	{
		ThrowIfFailed(D3D12CreateDevice(nullptr, D3D_FEATURE_LEVEL_11_0, IID_PPV_ARGS(&device)));
	}
}

void HelloWindow::InitCommandQueue()
{

	//TODO how does the command queue work and what do the paramters mean
	//There are for parameters 
	//Type - Specfices a member of the D3D12_COMMAND_LIST_TYPE
	//Including the following: 
	//D3D12_COMMAND_LIST_TYPE_DIRECT, specifies a command buffer that the GPU can execute. A direct command list doesn't inherit any GPU state
	//D3D12_COMMAND_LIST_TYPE_BUNDLE, Can only be executed via a direct command list. A bundle command list inheirts all GPU state (except for the current set pipeline state object and primitive topo) 
	//D3D12_COMMAND_LIST_TYPE_COMPUTE, Specifies a command buffer for computing
	//D3D12_COMMAND_LIST_TYPE_COPY Specifies a command buffer for copying(drawing)
	//Priority - This can be set to normal or high priority using the D3D12_COMMAND_QUEUE_PRIORITY enum
	//D3D12_COMMAND_QUEUE_PRIORITY_NORMAL = 0 D3D12_COMMAND_QUEUE_PRIORITY_HIGH = 100
	//Flags - Specifies any flags from the D3D12_COMMAND_QUEUE_FLAGS enum 
	//D3D12_COMMAND_QUEUE_FLAG_NONE = 0, the default command queue
	//D3D12_COMMAND_QUEUE_FLAG_DISABLE_GPU_TIMEOUT = 0x1,  indicates that eh GPU timeout should be disabled for this command queue
	//NodeMask - For single GPU operation set this to zero. If there are multiple GPU Nodes set a bit to identify the node
	//each bit corresponds to a single node.
	D3D12_COMMAND_QUEUE_DESC queueDesc = {};
	queueDesc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;
	queueDesc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
	queueDesc.Priority = D3D12_COMMAND_QUEUE_PRIORITY_NORMAL;
	queueDesc.NodeMask = 0x1;

	ThrowIfFailed(device->CreateCommandQueue(&queueDesc, IID_PPV_ARGS(&commandQueue)));
}

void HelloWindow::InitSwapChain()
{
	//TODO learn the parameters of the swap chain desc
	DXGI_SWAP_CHAIN_DESC swapChainDesc = {};
	swapChainDesc.BufferCount = FRAME_COUNT;
	swapChainDesc.BufferDesc.Width = width;
	swapChainDesc.BufferDesc.Height = height;
	swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
	swapChainDesc.SampleDesc.Count = 1; //multi-sampling?
	swapChainDesc.OutputWindow = hWnd;
	swapChainDesc.Windowed = TRUE;

	ComPtr<IDXGISwapChain> swapChain;
	ThrowIfFailed(factory->CreateSwapChain(commandQueue.Get(), &swapChainDesc, &swapChain));
	ThrowIfFailed(swapChain.As(&this->swapChain));
	frameIndex = this->swapChain->GetCurrentBackBufferIndex();
}

void HelloWindow::InitDescriptorHeap()
{
	//TODO what does that even mean
	//Create descriptor heaps 
	{
		D3D12_DESCRIPTOR_HEAP_DESC rtvHeapDesc = {};
		//number of descriptors in the heap
		rtvHeapDesc.NumDescriptors = FRAME_COUNT; 
		//Specifies the types of descriptors in the heap
		//D3D12_DESCRIPTOR_HEAP_FLAG_NONE default usage of a heap
		//D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE - specifies the heap is bound on a command list for reference by shaders.
		rtvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
		//The type of the descriptor heap
		//D3D12_DESCRIPTOR_HEAP_TYPE_RTV - render target view descriptor heap
		//D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV- The descriptor heap for the combination of constant-buffer,
		//shader-resource, and unordered-access views
		//D3D12_DESCRIPTOR_HEAP_TYPE_DSV - The descriptor heap for the depth stencil view
		//D3D12_DESCRIPTOR_HEAP_TYPE_NUM_TYPES - The number of types of descriptor heaps
		//D3D12_DESCRIPTOR_HEAP_TYPE_NUM_TYPES = the number of types of descriptor heaps
		rtvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
		ThrowIfFailed(device->CreateDescriptorHeap(&rtvHeapDesc, IID_PPV_ARGS(&rtvHeap)));
		rtvDescriptorSize = device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
	}

	//Create the frames resources
	{
		CD3DX12_CPU_DESCRIPTOR_HANDLE rtvHandle(rtvHeap->GetCPUDescriptorHandleForHeapStart());

		for (size_t i = 0; i < FRAME_COUNT; i++)
		{
			ThrowIfFailed(swapChain->GetBuffer(i, IID_PPV_ARGS(&renderTargets[i])));
			device->CreateRenderTargetView(renderTargets[i].Get(), nullptr, rtvHandle);
			rtvHandle.Offset(1, rtvDescriptorSize);
		}
	}
}

void HelloWindow::InitCommandAllocator()
{
	ThrowIfFailed(device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(&commandAllocator)));
}
